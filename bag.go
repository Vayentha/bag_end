package main

import (
	"./vega"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"sync"
	"syscall"
)

func main() {

	dateCmd := exec.Command("date")

	dateOut, err := dateCmd.Output()
	if err != nil {
		panic(err)
	}
	fmt.Println("> date")
	fmt.Println(string(dateOut))

	grepCmd := exec.Command("grep", "hello")

	grepIn, _ := grepCmd.StdinPipe()
	grepOut, _ := grepCmd.StdoutPipe()
	grepCmd.Start()
	grepIn.Write([]byte("hello grep\ngoodbye grep"))
	grepIn.Close()
	grepBytes, _ := ioutil.ReadAll(grepOut)
	grepCmd.Wait()

	fmt.Println("> grep hello")
	fmt.Println(string(grepBytes))

	lsCmd := exec.Command("bash", "-c", "ls -a -l -h")
	lsOut, err := lsCmd.Output()
	if err != nil {
		panic(err)
	}
	fmt.Println("> ls -a -l -h")
	fmt.Println(string(lsOut))
	//gsignal()
	//genSC()
	//gsignal()
	//BuildRunes()
	gg := vega.Cohong64{}
	fmt.Println(gg)
	pp := vega.Runedex{}
	fmt.Println(pp)
	pp.SetRunes("biggest gay")
	fmt.Println(pp)
	fmt.Println(pp.CHG)
	Make(256)
}

func genSC() {


		binary, lookErr := exec.LookPath("ls")
		if lookErr != nil {
			panic(lookErr)
		}

		args := []string{"ls", "-a", "-l", "-h"}	//put system call here

		env := os.Environ()

		execErr := syscall.Exec(binary, args, env)
		if execErr != nil {
			panic(execErr)
		}

}

//STRANGE SHIT
//will await an os signal.
func gsignal() {
		sigs := make(chan os.Signal, 1)
		done := make(chan bool, 1)

		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

		go func() {
			sig := <-sigs
			fmt.Println()
			fmt.Println(sig)
			done <- true
		}()

		fmt.Println("awaiting signal")
		<-done
		fmt.Println("exiting")

}

type msg struct {
	v  interface{}
	ok bool
}

// C is a channel
type C interface {
	// Send a messge to the channel. Returns false if the channel is closed.
	Send(v interface{}) (ok bool)
	// Recv a messge from the channel. Returns false if the channel is closed.
	Recv() (v interface{}, ok bool)
	// Close the channel. Returns false if the channel is already closed.
	Close() (ok bool)
	// Wait for the channel to close. Returns immediately if the channel is
	// already closed
	Wait()
}

type c struct {
	mu     sync.Mutex
	cond   *sync.Cond
	c      chan msg
	closed bool
}

// Make new channel. Provide a length to make a buffered channel.
func Make(length int) C {
	c := &c{c: make(chan msg, length)}
	c.cond = sync.NewCond(&c.mu)
	return c
}

func (c *c) Send(v interface{}) (ok bool) {
	defer func() { ok = recover() == nil }()
	c.c <- msg{v, true}
	return
}

func (c *c) Recv() (v interface{}, ok bool) {
	select {
	case msg := <-c.c:
		return msg.v, msg.ok
	}
}

func (c *c) Close() (ok bool) {
	c.mu.Lock()
	defer c.mu.Unlock()
	defer func() { ok = recover() == nil }()
	close(c.c)
	c.closed = true
	c.cond.Broadcast()
	return
}

func (c *c) Wait() {
	c.mu.Lock()
	defer c.mu.Unlock()
	for {
		if c.closed {
			return
		}
		c.cond.Wait()
	}
}